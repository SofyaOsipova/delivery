from rest_framework.serializers import ModelSerializer

from .models import Restaurant, Product


class RestaurantSerializer(ModelSerializer):
    class Meta:
        model = Restaurant
        fields = ['id', 'restaurant_name', 'restaurant_rating']


class ProductSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'description', 'product_name', 'product_price']

from django.urls import path
from . import views


app_name = 'delivery'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:restaurant_id>/', views.detail, name='detail'),
    path('info/<int:product_id>', views.info, name='info'),
    path('basket', views.basket, name='basket'),
    path('register/', views.register, name='register'),
    path('login/', views.login_user, name='login'),
    path('logout/', views.logout_user, name='logout'),
    path('basket_adding/', views.basket_adding, name='basket_adding'),
]

# path('rest-list/', main_rest),
# path('rest-list/<int:rest_id>/prod_list/', views.rest_detail),
# path('rest-list/<int:rest_id>/prod_list/<int:prod_id>/prod_info/', views.prod_detail)
# urlpatterns += router.urls

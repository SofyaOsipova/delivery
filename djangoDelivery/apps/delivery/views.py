from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.http import Http404, HttpResponseRedirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt, csrf_protect

from .models import Restaurant, Product, ProductInBasket
from .forms import CreateUserForm
from django.contrib import messages

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
# from .serializers import RestaurantSerializer, ProductSerializer


def register(request):
    if request.user.is_authenticated:
        return redirect('delivery:index')
    else:
        form = CreateUserForm()
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, f'Аккаунт для {user} создан!')
                return redirect('delivery:login')
        context = {'form': form}
        return render(request, 'delivery/register.html', context)


def login_user(request):
    if request.user.is_authenticated:
        return redirect('delivery:index')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username,  password=password)
            if user is not None:
                login(request, user)
                return redirect('delivery:index')
            else:
                messages.info(request, 'Неверный логин или пароль')
        context = {}
        return render(request, 'delivery/login.html', context)


def logout_user(request):
    logout(request)
    return redirect('delivery:login')


@login_required(login_url='login')
def index(request):
    restaurant_list = Restaurant.objects.all()
    return render(request, 'delivery/list.html', {'restaurant_list': restaurant_list})


@login_required(login_url='login')
def detail(request, restaurant_id):
    try:
        rest = Restaurant.objects.get(id=restaurant_id)
    except:
        raise Http404('Ресторан не найден!')

    products_list = rest.product_set.all()
    return render(request, 'delivery/detail.html', {'restaurant': rest, 'products_list': products_list})


@login_required(login_url='login')
def info(request, product_id):
    try:
        product = Product.objects.get(id=product_id)
    except:
        raise Http404('Блюдо не найдено!')

    return render(request, 'delivery/info.html', {'product': product, 'rest_id': product.restaurant})


@login_required(login_url='login')
def basket(request):
    return render(request, 'delivery/basket.html')


def basket_adding(request):
    return_dict = {}
    print('!!!')
    print(request.POST)
    data = request.POST
    product_id = data.get('product_id')
    num = data.get('num')
    user_id = request.user.id
    is_delete = data.get('is_delete')
    if is_delete == 'true':
        ProductInBasket.objects.filter(id=product_id).update(is_active=False)
    else:
        new_product, created = ProductInBasket.objects.get_or_create(user_id=user_id,
                                                                     product_id=product_id, defaults={'num': num},
                                                                     is_active=True)
        if not created:
            new_product.num += int(num)
            new_product.save(force_update=True)

    products_total_num = ProductInBasket.objects.filter(user_id=user_id, is_active=True).count()
    return_dict['products_total_num'] = products_total_num
    print('!!!!!!', return_dict)
    return JsonResponse(return_dict)
